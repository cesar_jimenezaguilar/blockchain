
const SHA256 = require('crypto-js/sha256');

function Block(index, data, previousHash, difficulty) {
    this.index = index;
    this.date = new Date().toString();
    this.data = data;
    this.previousHash = previousHash;
    this.nonce = 0;
    this.hash = createHash(index, data, this.date, previousHash, this.nonce, difficulty);
}

function createHash(index, data, date, previousHash, nonce, difficulty) {
    let hash = '';
    while (!hash.startsWith(difficulty)) {
        nonce++;
        hash = SHA256(index + data + date.toString() + previousHash + nonce).toString();
    }
    return hash;
}

function createHashNoValid(index, data, date, previousHash, nonce) {
    return SHA256(index + data + date.toString() + previousHash + nonce).toString();
}

function mine(block, difficulty) {
    block.hash = SHA256(block.index, block.data, block.date, block.previousHash, block.nonce).toString();

    while (!block.hash.startsWith(difficulty)) {
        block.nonce++;
        block.hash = createHashNoValid(block.index, block.data, block.date, block.previousHash, block.nonce);
    }
    return block;
}

function createFirstBlock(data, difficulty) {
    return new Block(0, data, '', difficulty);
}

function isValid(chain, difficulty) {
    for (let i = 1; i < chain.length; i++) {
        let prevBlock = chain[i - 1];
        let currBlock = chain[i];
        if (currBlock.previousHash != prevBlock.hash) {
            return false;
        }
        const h = createHash(currBlock.index, currBlock.data, currBlock.date, currBlock.previousHash, 0, difficulty);
        if (h != currBlock.hash) {
            return false;
        }

    }
    return true;
}

function amountAvailable(chain, data) {
    let sum = parseInt(amount(chain)) + parseInt(data);
    console.log(sum);
    if (sum >= 0) {
        return true;
    }
    return false;
}

function amount(chain) {
    let sum = 0;
    for (let i = 0; i < chain.length; i++) {
        sum += parseInt(chain[i].data);
    }
    return sum;
}

module.exports = {
    Block,
    mine,
    createFirstBlock,
    isValid,
    amountAvailable,
    amount

};

//block = new Block(0, 'prueba');
//console.log(JSON.stringify(block, null, 2));

//let naniCoin = new Blockchain('genesis');

//naniCoin.addBlock('info 2');
//aniCoin.addBlock('info 3');


//console.log(naniCoin.isValid());
//console.log(JSON.stringify(naniCoin, null, 2));
//naniCoin.chain[1].data = 'fake data';
//console.log(naniCoin.isValid());