let blocks = require('../block.js');
let Block = blocks.Block;
var express = require('express');
var router = express.Router();
var fs = require('fs');
const url = 'C:/Users/cesar/Desktop/planificacion/investigacion/blockchainReact/blockchain/blocks.json';
const difficulty = '00';

/* GET home page. */
router.get('/', function (req, res) {
  let chain = fs.readFileSync(url);
  chain = JSON.parse(chain);
  res.send(chain);
});

/* GET home page. */
router.get('/getAmount', function (req, res) {
  let chain = fs.readFileSync(url);
  chain = JSON.parse(chain);
  res.send({ amount: blocks.amount(chain) });
});

/*POST add block*/
router.post('/addBlock', function (req, res) {
  let block = null;
  let chain = JSON.parse(fs.readFileSync(url));
  const length = chain.length;
  const data = req.body.amountInput;
  if ((blocks.isValid(chain, difficulty) || length == 0) && data != 0) {
    const accepted = blocks.amountAvailable(chain, data);
    if (accepted == true) {
      if (length == 0) {
        block = blocks.createFirstBlock(parseInt(req.body.amountInput, 10), difficulty);
      } else {
        block = new Block(chain[length - 1].index + 1, parseInt(data, 10), chain[length - 1].hash, difficulty);
      }
      block = blocks.mine(block, difficulty);
      chain.push(block);
      fs.writeFileSync(url, JSON.stringify(chain), 'utf-8');
      res.send('ok');

    } else {
      console.log('denied')
      res.send('denied');
    }
  } else {
    console.log('error');
    res.send('error');
    return;
  }
});

router.get('/isValid', function (req, res) {
  let chain = JSON.parse(fs.readFileSync(url));
  const length = chain.length;
  if (length != 0) {
    const isValid = blocks.isValid(chain, difficulty);
    res.send(isValid);
  }

  res.send('empty');
});

router.post('/addBlock1', function (req, res) {

});

module.exports = router;
