class Blockchain {
    constructor(genesis) {
        this.chain = [this.createFirstBlock(genesis)];
        this.difficulty = '00';

    }

    getLastBlock(chain) {
        return chain[chain.length - 1];
    }

    addBlock(data) {
        let prevBlock = this.getLastBlock();
        let block = new Block(prevBlock.index + 1, data, prevBlock.hash);
        block.mine(this.difficulty);
        console.log('minado ' + block.hash + ' con nonce ' + block.nonce);
        this.chain.push(block);
    }

    isValid() {
        for (let i = 1; i < this.chain.length; i++) {
            let prevBlock = this.chain[i - 1];
            let currBlock = this.chain[i];


            if (currBlock.previousHash != prevBlock.hash) {
                console.log("se cae 1");
                return false;
            }

            if (currBlock.createHash() != currBlock.hash) {
                console.log("se cae 2");
                return false;
            }

        }
        return true;
    }
}